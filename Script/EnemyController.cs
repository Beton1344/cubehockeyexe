/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	Transform player;
	BallLife ballLife; 
	int actionRandom; 
	float delay = 5.0f; 
	float timeCollision = 0;
	public bool isCollided; 
	float bruteForce;
	float bruteFactor = 1; 
	// Use this for initialization
	void Start () {
		player = GameObject.Find("Ball").GetComponent<Transform>();  
		ballLife = GameObject.Find ("Ball").GetComponent<BallLife>(); 
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.LookAt(player); 
		actionRandom = Random.Range(1, 1000);
		if( actionRandom > 990){
			move(); 
		}
		if(	actionRandom % 850 == 0 ){
			attack ();
		}
		if(Input.GetKey(KeyCode.Y)){
			attack ();
		}
	}

	void move() {
		this.rigidbody.AddForce(transform.rotation.eulerAngles/10,ForceMode.Force);
		
	}
	void attack() {
		this.rigidbody.AddForce ( 1 * (player.position - this.transform.position), ForceMode.Force);
			
	}
	
	void OnCollisionEnter(Collision other){
		if(other.collider.name == "WingMan"){
			isCollided = true; 
			if(Time.time > timeCollision){
				timeCollision = Time.time + delay; 
				bruteForce = other.impactForceSum.magnitude; 
				Debug.Log ("Brute force is " + bruteForce.ToString());
				ballLife.GainLife (Mathf.RoundToInt(bruteForce * bruteFactor));
			}
			
		} else if (other.collider.name.Equals("FloorModel") || other.collider.name.Equals ("Sphere")) {
			
		} else {
			isCollided = false; 
			
		}
		
		
	}
}
