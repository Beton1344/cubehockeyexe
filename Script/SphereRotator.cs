/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class SphereRotator : MonoBehaviour {
	public float rotationSpeed = 10; 
	// Use this for initialization
	void Start () {
		rotationSpeed = Random.Range (7, 15); 
		rotationSpeed *= (Random.Range (1,3) < 2 ? -1 : 1); 
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up * Time.deltaTime * rotationSpeed);
	}
}
