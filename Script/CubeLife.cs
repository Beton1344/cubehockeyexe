/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class CubeLife : MonoBehaviour
{
	BallLife ballLife;
	ReturnToTop returnToTop; 
	bool goalis;
	public string lastTouchedName; 
	
	

	// Use this for initialization
	void Start ()
	{
		ballLife = GameObject.Find ("Ball").GetComponent<BallLife> (); 
		returnToTop = this.gameObject.GetComponent<ReturnToTop> (); 
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (this.transform.position.y < 8) {
			
			if(goalis){
				ballLife.Goal (); 
				goalis = false;
			}
		} 
	
	}
	
	void OnCollisionEnter(Collision other){
		if(other.gameObject.name.Equals ("GoalRecord"))	{
			goalis = true;	
		}
		lastTouchedName = other.gameObject.name;
		
	}
}
