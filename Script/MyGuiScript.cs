/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class MyGuiScript : MonoBehaviour {
	BallLife ballLife; 
	public GUIStyle myStyle = new GUIStyle(); 
	// Use this for initialization
	void Start () {
		ballLife = GameObject.Find ("Ball").GetComponent<BallLife>(); 

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI(){
		GUI.Box (new Rect(15, 15, 150, 200), ballLife.Message(), "box");
	}
}
