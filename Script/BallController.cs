/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {
	// public Prefab ball; 
	// Use this for initialization
	public PhysicMaterial[] physics; 
	public Texture2D[] textures; 
	
	public float torquePower; 
	public float actualTorque;
	public float powerTorque; 
	public Vector3 direction; 
	
	BallLife ballLife; 
	
	void Start () {
		actualTorque = 350.0f; 
		torquePower = 100;
		ballLife = this.GetComponent<BallLife>(); 
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if( transform.position.y < -10){
			transform.position = Vector3.up * 10;
		}
		powerTorque = 1.0f; 
		
		if(!ballLife.GameOver()){
			if (Input.GetKey (KeyCode.Space)){
				powerTorque = 100.0f; 
			}
			if (Input.GetKey (KeyCode.UpArrow)){
				this.rigidbody.AddTorque ((Vector3.right * actualTorque * powerTorque));
			} 
			if (Input.GetKey (KeyCode.RightArrow)){
				this.rigidbody.AddTorque ((Vector3.back * actualTorque * powerTorque));
	
			} 
			if (Input.GetKey (KeyCode.LeftArrow)){
				this.rigidbody.AddTorque((Vector3.forward * actualTorque * powerTorque));
					
			} 
			if (Input.GetKey (KeyCode.DownArrow)){
				this.rigidbody.AddTorque ((Vector3.left * actualTorque * powerTorque));
		
			}
		}
		if (Input.GetKey (KeyCode.P)){
			Application.LoadLevel (Application.loadedLevel);
		}
	}
	
}
