/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class BallLife : MonoBehaviour {
	float life = 100.0f;
	int goal = 0;
	public GUIStyle gs;
	public GUIStyle go; 
	float goalDelay = 5.0f;
	float goalLife = 50.0f; 
	float goalTime = 0; 
	float fallTime = 0; 
	bool gameOver = false; 
	string gameOverMsg = "Game over\nPress P\nto play again"; 

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if( this.transform.position.y < -8 ) {
			Fall (); 
			if( life < 0 ) 
				gameOver = true; 
		}
		
	}
	
	public void LoseLife(float damage){
		life -= damage;
		//Debug.Log( "damage is " + damage.ToString () + "life is " +  life.ToString());
	}
	public void GainLife(float bonus) {
		life += bonus; 
		//Debug.Log( "bonus is " + bonus.ToString () + "life is " +  life.ToString());
	}
	void OnCollisionEnter(Collision other) {
		if( other.rigidbody != null ){
			if ( other.gameObject.name.Equals("Enemy") ){
				LoseLife(10); 
				
			}
		}
	}
	
	public void Fall(){
		if(Time.time > fallTime){
			fallTime = Time.time + goalDelay;
			LoseLife (25);
		}
		
	}
	public void Goal(){
		if(Time.time > goalTime){
			goal += 1;
			goalTime = Time.time + goalDelay; 
			GainLife(goalLife); 
			Debug.Log ("Goal!!!!");
		}
	}
	public void Rejuvenate(){
		Debug.Log("Rejuvenate() is called"); 
		if( life < 0 )
			life = 17; 
	}
	
	
	
	public string Message(){
		return "G::: " + goal + "\n" +  "Life: " + life;
	}
	
	public bool GameOver(){
		return gameOver; 
	}
	
	void OnGUI() {
		GUI.Box (new Rect(10, 10, 150, 150), this.Message (), gs);
		if(gameOver)
		GUI.Box (new Rect(110, 150, 500, 300), gameOverMsg, go); 
	}	

}
