/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class CameraControls : MonoBehaviour {
	public float zoomPower;
	public Transform playerPosition; 
	
	bool rotateMode; 
	float rotationPower; 
	public float speedLevel = 100; 
 
	Quaternion actualRotation; 
	float[] zoomLimits = new float[2]; // Use this for initialization
	private Vector3 offset;
	private Vector3 offsetRotate;
	private Vector3 temp; 
	private float yData; 
	void Start () {
		rotationPower = 150;
		offset = transform.position - playerPosition.transform.position; 
		zoomPower = 150.0f;
		zoomLimits[0] = 30.0f; 
		zoomLimits[1] = 87.0f; 
	}

	void LateUpdate() {
 			this.transform.LookAt(playerPosition); 
		
		
	}
	void Update() {
		temp = transform.position - playerPosition.transform.position; 
		offsetRotate = new Vector3(temp.x, this.transform.position.y, temp.z);   //( temp.x, this.transform.position.y, temp.z); 
		if(Input.GetKey (KeyCode.A)){
			// rotate around player left
			this.transform.RotateAround(playerPosition.position, Vector3.up, rotationPower * Time.deltaTime);
		} else if (Input.GetKey (KeyCode.D)){
			// rotate around player rigt
			this.transform.RotateAround(playerPosition.position, Vector3.up, rotationPower * Time.deltaTime * -1); 
		} else if (Input.GetKey (KeyCode.S)){
			// go from the player
			if(camera.fieldOfView > zoomLimits[0]){
			this.camera.fieldOfView -= zoomPower * Time.deltaTime; 
			} else {
				this.camera.fieldOfView = zoomLimits[0]; 
			}
		} else if (Input.GetKey (KeyCode.W)){
			if(camera.fieldOfView < zoomLimits[1]){
				this.camera.fieldOfView += zoomPower * Time.deltaTime;
			} else {
				this.camera.fieldOfView = zoomLimits[1];
			}
		} else if ( Input.GetKeyDown (KeyCode.Q) ){
			if(rotateMode)
				rotateMode = false;
			else
				rotateMode = true; 
		}
		
		if(Input.GetKey (KeyCode.Space)){
			this.transform.position += Vector3.up * speedLevel * Time.deltaTime; 
		}else if (Input.GetKey (KeyCode.LeftShift)){
			this.transform.position += Vector3.down * speedLevel * Time.deltaTime;
		}
		
		actualRotation = this.transform.rotation; 
		if(!rotateMode) {
			transform.position = playerPosition.transform.position + offset;
		} else {
			//transform.position = playerPosition.transform.position + offsetRotate; 
		}
		//transform.rotation = actualRotation;

	
		
	}
}
