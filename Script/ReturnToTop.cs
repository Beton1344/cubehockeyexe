/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class ReturnToTop : MonoBehaviour {
	public float tolerance = -10f; 
	public float falloffDrag = 5.0f;
	float originalDrag; 
	// Use this for initialization
	void Start () {
		originalDrag = rigidbody.drag;
		if(this.gameObject.name.Equals ("Biggy"))
			tolerance = -30; 
		
	}
	
	// Update is called once per frame
	void Update () {
		if( transform.position.y < tolerance) {
			transform.position = Vector3.up * 10; 
			rigidbody.drag = falloffDrag;
			transform.rigidbody.velocity = Vector3.zero;
		}
		
		if( transform.position.y > 4 ){
			rigidbody.drag = originalDrag; 
		}
		
	
	}
}
