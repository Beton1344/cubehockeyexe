/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class dynamicManController : MonoBehaviour {

	public int squarePosition = 15; // le coins visé
	public int seuil = 12; 
	public float bruteForce = 300.0f; 
	public bool inPosition = false; 
	public bool isTouched = false;
	public Vector3 destination; 
	public float timeBeforeJump = 2;  
	public float timeCompare; 
	int negRand1, negRand2;
	// Use this for initialization
	void Start () {
		timeCompare = Time.time + timeBeforeJump; 
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.rigidbody.AddForce (destination - this.transform.position);
		if( inPosition ){
			
			inPosition = false; 
			negRand1 = ( Random.Range (1,3) == 1 ? -1 : 1 ); 
			negRand2 = ( Random.Range (1,3) == 1 ? -1 : 1 ); 
			destination = new Vector3(negRand1 * squarePosition, 1, negRand2 * squarePosition); 
			Debug.Log("Dans inPosition, destination : " + destination.ToString ()); 
		} else {
			if( Vector3.SqrMagnitude(this.transform.position - destination) < seuil ){
				inPosition = true; 
				Debug.Log ("Arrivé a destination" + this.transform.position.ToString ());
			}
			if(!isTouched && Time.time > timeCompare){
				timeCompare = Time.time + timeBeforeJump; 
				this.transform.rigidbody.AddForce((destination - this.transform.position )*Time.deltaTime * bruteForce); 
			} else {
				destination = destination * -1; 
				
			}
		}
	}
	
	void OnCollisionEnter(Collision other){
		if(other.collider.name == "Ball"){ 
			isTouched = true; 
		} 
	}
	
	void OnCollisionExit(Collision other){
		if(other.collider.name == "Ball"){
			isTouched = false;
		}
		
	}
}
