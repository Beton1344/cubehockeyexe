/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class Floor : MonoBehaviour {
	Material materialOriginal; 
	public Material materialGoal;
	bool showGoal; 
	public float shrink = 0.8f; 
	public float timeScale = 300000.0f;
	float tolerance = 1; 
	float born; 
	float nowMod;
	Vector3 originalScale; 
	// Use this for initialization
	void Start () {
		materialOriginal = this.gameObject.renderer.material; 
		originalScale = transform.localScale; 
		born = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
		if( showGoal ){
			this.renderer.material = materialGoal; 	
		} else {
			this.renderer.material = materialOriginal;
		}

	}
/*	void LateUpdate(){
		if( showGoal ){
			this.renderer.material = materialGoal; 	
		} else {
			this.renderer.material = materialOriginal;
		}
	}*/
	
	public void ShowGoal(){
		showGoal = true; 
		
		
	}
	
	public void ShowOriginal(){
		showGoal = false; 
	}
}
