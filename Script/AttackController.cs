/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class AttackController : MonoBehaviour {
	public Transform player; 
	float time; 
	float originalTolerance = 10.0f;
	float tolerance;
	float distance = 12;  
	BallLife ballLife; 
	Vector3 direction; 

	// Use this for initialization
	void Start () {
		tolerance = Time.time + originalTolerance; 
		player = GameObject.Find("Ball").transform;
		ballLife = player.GetComponent<BallLife>(); 
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ((this.transform.position - biggy.position).sqrMagnitude);
		if ( Time.time > tolerance ) {
			///Debug.Log ("Referree is moving");
			direction = player.position - transform.position; 
			if( (this.transform.position - player.position).sqrMagnitude < distance){
				direction = direction * -4; 
			}
			//Debug.Log( "Moving to : " + direction.ToString());
			this.rigidbody.AddForce(direction * (Random.Range (0.7f, 1.5f) )); 
			time = Time.time; 
			tolerance = originalTolerance * Random.Range (0.5f, 1.3f);
		
			
		}
	}
}
