/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class TextControler : MonoBehaviour {
	public Transform camera; 
	public BallLife ballLife; 

	// Use this for initialization
	void Start () {
		//camera = GameObject.Find ("Camera").transform;
		ballLife = GameObject.Find ("Ball").GetComponent<BallLife>(); 
	}
	
	// Update is called once per frame
	void Update () {
		//this.transform.LookAt(camera); 
		this.guiText.text = ballLife.Message (); 
	
	}
}
