/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class TroubleMakerController : MonoBehaviour {
	BallLife ballLife; 
	float timeFall = 6; 
	float timeFalled; 
	// Use this for initialization
	void Start () {
		ballLife = GameObject.Find ("Ball").GetComponent<BallLife>(); 
		timeFalled = Time.time + timeFall; 
	}
	
	// Update is called once per frame
	void Update () {
		if(this.transform.position.y < -8  ){
			timeFalled = Time.time + timeFall; 
			ballLife.Rejuvenate(); 
		}
	}
}
