/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class SpasmsControler : MonoBehaviour {
	float bounceTime; 
	Component cubeLife; 
	// Use this for initialization
	void Start () {
		bounceTime = Time.time; 
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		 
		if ( Time.time > bounceTime ){
			rigidbody.AddRelativeTorque(Random.rotation.eulerAngles); 	
			SetBounceTime ();
		}
	
	}
				
	void SetBounceTime(){
				bounceTime = Time.time +  Random.Range(2.0f, 5.0f); 
	
	}
}
