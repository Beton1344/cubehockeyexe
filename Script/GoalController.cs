/**************************
 * Projet 3d - inf5071 - hiver 2016
 * Prof : Alexandre Blondin-Massé
 * Titre : Cube Hockey
 * Auteur : Eric Lavallee - LAVE23077505
 * Licences : GPL
 **************************/

using UnityEngine;
using System.Collections;

public class GoalController : MonoBehaviour {
	CubeLife cubeLife;
	public float timeOfSleep = 10.0f; 
	public float timeOfFlash = 3.0f;
	public float timeOfZoom = 1.5f; 
	public float lastZoomed; 
	public float originalZoom; 
	public float minZoom = 10.0f; 
	public float maxZoom = 50.0f; 
	BallLife ballLife;
	Camera camera; 
	float lastTouched; 
	float lastGoal = 0.0f; 
	string scoreName; 
	public Color originalColor; 
	public Color colorStart = new Color(255, 0, 0); 
	public Color colorEnd = new Color(255, 65, 65); 
	public Color colorMiddle = new Color(255, 255, 0); 
	public float duration = 0.5f; 
	public float zoomDuration = 0.3f; 
	
	Floor floor; 

	// Use this for initialization
	void Start () {
		floor = GameObject.Find("Arena").GetComponent<Floor>(); 
		originalColor = renderer.material.color;
		ballLife = GameObject.Find("Ball").GetComponent<BallLife>(); 
		cubeLife = GameObject.Find("Biggy").GetComponent<CubeLife>();
		lastTouched = Time.time + 10.0f;
		lastZoomed = Time.time + 5.0f; 
		camera = GameObject.Find ("Main Camera").GetComponent<Camera>();
		originalZoom = camera.fieldOfView; 
		
	}
	
	// Update is called once per frame
	void Update () {


		if(Time.time < lastTouched && lastGoal != 0.0f){

			
		float lerp = Mathf.PingPong (Time.time, duration) / duration; 
		if( Mathf.RoundToInt(lerp * 100) % 2 == 0) floor.ShowGoal(); else floor.ShowOriginal(); 
		renderer.material.color = Color.Lerp (colorStart, colorEnd, lerp); 
		
		} else if( lastGoal != 0.0f && Time.time > lastTouched) {
			lastGoal = 0.0f;
			renderer.material.color = originalColor; 
			floor.ShowOriginal ();
			

		} 
		if(Time.time >= lastGoal && Time.time < lastTouched)
			floor.ShowOriginal (); 
	}
	
	void OnCollisionEnter(Collision other) {
		if(other.gameObject.name.Equals ("Biggy")){
			if(Time.time > lastTouched){
				scoreName = other.gameObject.name; 
				Debug.Log ("Scorename = " + scoreName);
				lastTouched = Time.time + timeOfSleep; 
				lastGoal = Time.time + timeOfFlash; 
			}
		}
	}
	
	
}
