****************************************
Cube Hockey 
Le jeu de hockey spécial ou spatial!
Où un cube est un puck et une sphère, un joueur...
****************************************

Auteur : Eric Lavallee

Ce projet a été créé dans le cadre du cours inf5071 - infographie, donnée
à l'automne 2016 par Alexandre Blondin-Massé. Ce cours à été donnée dans
l'enceinte somptueuse de l'UQAM, à Montréal. 

****************************************
LE JEU 
****************************************

Le but du jeu est de pousser un cube avec une sphere vers un des deux 
buts posé aux extrémité à gauche et à droites. 

Des ennemis verts vous donne du dégat (-10) lors de contacts, un "alié" 
gris peut vous aider ou vous nuire, c'est selon. 

Un soleil, au centre agit comme distributeur et une structure, fait de 
deux pyramides juxtaposé sert de revigorant, vous donnant un 17 points 
de vie, si vous réussisser à le pousser à l'extérieur de la plateforme de jeu. 

****************************************
LES CONTRÔLES
****************************************

Pour diriger la boules:
Les quatres flêches dirige la sphère dans les direction associés. 

Pour gerer la camera:
  S : zoom in
  W : zoom out
  Q : alterner entre mode rotation/fixe
  A : rotation à gauche (mode rotation)
  D : rotation à droite (mode rotation)

Pour remettre à zéro : 
  P : "resseter le niveau" 

*****************************************
Exécution
*****************************************

Les trois plateformes (Windows, Linux, MAC) sont
supportée par leurs exécutables fournis ici. 

*****************************************
Les fichiers présentées
*****************************************

BlendFiles
Le fichiers blenders utilisé pour la création des assets

Script
Les scripts .cs utilisé pour la création du jeu dans unity. 

Textures
Les fichiers de textures utilisé dans le jeu

BuildPresentation*****
Les trois "build" pour les plateformes associées

******************************************
LICENCES
******************************************

L'ensemble du projet est GPL, hormis le concept en soi, qui demeure 
la propriété de l'auteur. 


  
